#=========== Security group ==================
resource "aws_security_group" "test_sg" {
  name = "vault_test_sg"

  dynamic "ingress" {
    for_each = [
      "8200", //vault ui
      "22", //ssh
      "8080", //app
      "8081"] //app actuator
    content {
      from_port = ingress.value
      protocol = "tcp"
      to_port = ingress.value
      cidr_blocks = [
        "0.0.0.0/0"]
    }
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  tags = {
    Name = "vault_test_sg"
  }
}

#================= Role for EC2 with Vault ===================
resource "aws_iam_role" "vault_role" {
  name = "vault_role"
  # describes who can assume this role?
  assume_role_policy = file("who_can_assume_this_role.json")
  # https://www.vaultproject.io/docs/auth/aws#recommended-vault-iam-policy
  # https://github.com/hashicorp/terraform-aws-vault/tree/master/examples/vault-iam-auth
  /*
  The workflow is that the client trying to authenticate will create a request to the method GetCallerIdentity of the AWS STS API (but not yet send it). This method basically answers the question "Who am I?". This request is then signed with the AWS credentials of the client. The signed result is then sent with the login request to the Vault Server. When the Vault server receives a login request with the iam method, it can execute the STS request without actually knowing the contents of the signed part. It then receives a response from STS identifying who signed it, which the Vault Server then can check against the ARN of the IAM principal bounded to a previously created Vault Role and decide if it should be allowed to authenticate or not.
  */
  inline_policy {
    name = "my_inline_policy"
    policy = jsonencode({
      Version = "2012-10-17",
      Statement = [
        {
          Effect = "Allow",
          Action = [
            "ec2:DescribeInstances",
            "iam:GetInstanceProfile",
            "iam:GetUser",
            "iam:GetRole"
          ],
          Resource = "*"
        }]
    })
  }
}

resource "aws_iam_instance_profile" "vault_instance_profile" {
  name = "vault_instance_profile"
  role = aws_iam_role.vault_role.name
}

#============= Role for EC2 with App ===============
resource "aws_iam_role" "app_role" {
  name = "app_role"
  assume_role_policy = file("who_can_assume_this_role.json")
}

resource "aws_iam_instance_profile" "app_instance_profile" {
  name = "app_instance_profile"
  role = aws_iam_role.app_role.name
}

#=============== Vault EC2 ==================
resource "aws_instance" "vault" {
  ami = data.aws_ami.latest_amazon_linux_2.id
  instance_type = "t2.micro"
  user_data = templatefile("vaultUserData.tpl", {
    aws_account_id = data.aws_caller_identity.current.account_id,
    policy = file("policy.hcl"),
    aws_role_for_app = aws_iam_role.app_role.arn,
    aws_secret_key = var.aws_secret_key
    aws_access_key = var.aws_access_key
  })
  iam_instance_profile = aws_iam_instance_profile.vault_instance_profile.name
  tags = {
    Name = "vault-dev-server"
  }
  vpc_security_group_ids = [
    aws_security_group.test_sg.id]
  key_name = "kolyaiks_iam"
}

#=============== App EC2 ==================
resource "aws_instance" "app_server" {
  ami = data.aws_ami.latest_amazon_linux_2.id
  instance_type = "t2.micro"
  iam_instance_profile = aws_iam_instance_profile.app_instance_profile.name
  user_data = templatefile("appUserData.tpl", {
    vault_private_dns_name = aws_route53_record.vault_private_dns_name.fqdn
  })
  tags = {
    Name = "app-dev-sever"
  }
  vpc_security_group_ids = [
    aws_security_group.test_sg.id]
  key_name = "kolyaiks_iam"
}

# ====== Route 53 ============

resource "aws_route53_zone" "dns_private_zone" {
  name = "internal.kolyaiks"

  vpc {
    vpc_id = data.aws_vpc.default_vpc.id
  }
}

resource "aws_route53_record" "vault_private_dns_name" {
  zone_id = aws_route53_zone.dns_private_zone.id
  name    = "vault"
  type    = "A"
  ttl     = "30"
  records = [aws_instance.vault.private_ip]
}