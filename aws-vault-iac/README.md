# Example of usage the Vault AWS IAM Authentication method

1. We are setting up two servers: the first one is for the Vault server, and the second one is for the client app, which will use Vault. 
   Each server has its own AWS role linked.
2. Vault server is started in the dev mode as a background linux process.
3. We add a secret to the Vault KV v2 secret engine.
4. We write the Vault policy that allows reading the secret from the path "secret/spring-boot-app".
5. We enable the Vault AWS Auth method and create the Vault role "vault_role_for_app" inside it that will be used by the client to authenticate. 
   This Vault role is linked with AWS role of the client app server, and the Vault policy we've created at the previous step.
6. After launching this terraform config the Vault CLI client from app server logs in to the Vault server, retrieves the secret and writes it to /tmp/secret.txt.
7. Additionally, we can deploy spring-boot-app to the client application server to try Vault integration with Spring Boot application.
Launch command will be: 
```java -jar spring-boot-app-0.0.1-SNAPSHOT.jar``` 
   
More detailed process of communication between Vault client and Vault server is described below:

1. Vault client creates request with "GetCallerIdentity" method to AWS STS service (but doesn't send it to STS) and sings this request with AWS credentials.
2. Signed request concatenates with vault login request and goes from Vault client to the Vault server, instead of STS.
3. Vault server sends incoming request to AWS STS service to determine who is the source of the request. Depending on the response from STS Vault authenticates the client.

Useful links about this process:
```https://github.com/hashicorp/terraform-aws-vault/tree/master/examples/vault-iam-auth```
```https://www.vaultproject.io/docs/auth/aws#iam-auth-method```




