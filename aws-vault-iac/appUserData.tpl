#!/bin/bash
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum -y install vault
export VAULT_ADDR="http://${vault_private_dns_name}:8200"
vault login -method=aws role=vault_role_for_app
vault kv get secret/spring-boot-app > /tmp/secret.txt


sudo amazon-linux-extras install java-openjdk11 -y