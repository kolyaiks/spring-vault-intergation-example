variable "aws_secret_key" {
  description = "Enter the secret key of AWS account with appropriate rights"
  default = "xxx"
}

variable "aws_access_key" {
  description = "Enter the access key of AWS account with appropriate rights"
  default = "xxx"
}