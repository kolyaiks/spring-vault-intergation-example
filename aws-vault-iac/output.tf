output "vault" {
  value = map(aws_instance.vault.id, aws_instance.vault.public_ip)
}

output "vault_dns_key_value" {
  value = map(aws_route53_record.vault_private_dns_name.fqdn, aws_instance.vault.private_ip)
}

output "app_server" {
  value = map(aws_instance.app_server.id, aws_instance.app_server.public_ip)
}