#!/bin/bash
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum -y install vault
# to launch vault dev server in the background we use "&" in the end of the line
vault server -dev -dev-listen-address="0.0.0.0:8200" -dev-root-token-id="token" &
export VAULT_ADDR="http://127.0.0.1:8200"
vault login token
vault kv put secret/spring-boot-app name=name1 password=pass1
echo '${policy}' | vault policy write vault_policy_for_app_to_read_secret -
vault auth enable aws
vault write auth/aws/config/client secret_key=${aws_secret_key} access_key=${aws_access_key}
vault write auth/aws/role/vault_role_for_app auth_type=iam \
              bound_iam_principal_arn=${aws_role_for_app} policies=vault_policy_for_app_to_read_secret max_ttl=500h