# "data" is used because of KV v2 secret engine
path "secret/data/spring-boot-app" {
  capabilities = [
    "read",
    "list"]
}
# for UI
path "secret/*" {
  capabilities = [
    "list"]
}