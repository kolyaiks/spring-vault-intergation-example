## Vault and Spring Boot integration example 

This project contains two separate projects:

1. Terraform AWS infrastructure configuration to launch the Vault server and the client server.
2. Spring Boot application that must be deployed on a client server and can retrieve the secret from the Vault server.

Detailed information on each project can be found inside the project folder.