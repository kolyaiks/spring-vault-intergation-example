## Test Spring boot application with Vault and Actuator support

The main purpose of the application is to retrieve the secrets stored in Vault. 
We use the **spring-cloud-starter-vault-config** dependency with **AWS IAM authentication** to achieve that. 

Some details about AWS-IAM authentication in Vault: [hashicorp docs](https://www.vaultproject.io/docs/auth/aws#iam-auth-method)

Some details about AWS-IAM authentication support in
Spring: [spring docs](https://cloud.spring.io/spring-cloud-vault/reference/html/#vault.config.authentication.awsiam)

This **spring-cloud-starter-vault-config** dependency looks for the kv secrets at the default paths below:

```
/secret/{application}/{profile}
/secret/{application}
/secret/{default-context}/{profile}
/secret/{default-context}
```

so the actual path at Vault in our case must be:
`secret/spring-boot-app`
and it can be set in application via params below:

```
spring.application.name=spring-boot-app
spring.cloud.vault.kv.default-context=spring-boot-app
```

Secret stores two values - name and password. To retrieve
these values we can send a request below to our application:
```
curl "http://localhost:8080/get-secrets"
```
After refreshing the values in Vault we can refresh the values in our application by using the actuator feature. Request
to refresh the values:
```
curl -X POST -H "Content-Type: application/json"  http://localhost:8081/actuator/refresh
```

[iam-auth-method]: https://www.vaultproject.io/docs/auth/aws#iam-auth-method