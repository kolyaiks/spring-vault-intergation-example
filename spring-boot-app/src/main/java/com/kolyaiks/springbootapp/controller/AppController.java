package com.kolyaiks.springbootapp.controller;

import com.kolyaiks.springbootapp.dto.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AppController {

    private final Response response;

    @GetMapping(value = "/get-secrets")
    public Response handleRequest() {
        return response;
    }
}
