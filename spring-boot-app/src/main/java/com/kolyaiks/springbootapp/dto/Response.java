package com.kolyaiks.springbootapp.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@Data
@RefreshScope
@JsonIgnoreProperties({"targetClass", "targetSource", "targetObject", "advisors", "frozen", "exposeProxy", "preFiltered", "proxiedInterfaces", "proxyTargetClass", "advisorCount"})
public class Response {

    @Value("${name}")
    private String name;

    @Value("${password}")
    private String password;

}
